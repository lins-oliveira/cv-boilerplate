rodrigo-lins-resume.pdf : template.tex content.yml
	pandoc $(filter-out $<,$^ ) -o $@ --pdf-engine=xelatex --template=$<
	open rodrigo-lins-resume.pdf

clean :
	rm rodrigo-lins-resume.pdf
